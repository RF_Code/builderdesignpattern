﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Builder builder = new Builder();
            CarBuilder carBuilder = new CarBuilder();

            builder._carBuilder = carBuilder;

            Console.WriteLine("Basic car:");
            builder.BasicCar();
            Console.WriteLine(carBuilder.);

            Console.WriteLine("Standard full featured product:");
            director.BuildFullFeaturedProduct();
            Console.WriteLine(builder.GetProduct().ListParts());

            // Remember, the Builder pattern can be used without a Director
            // class.
            Console.WriteLine("Custom product:");
            builder.BuildPartA();
            builder.BuildPartC();
            Console.Write(builder.GetProduct().ListParts());
        }
    }

    public interface ICarBuilder
    {
        void Wheels();
        void CarDoors(int count);
        void ABS();
        void Radio();
        void Car();
        void MotionSensor();
    }

    public class CarBuilder : ICarBuilder
    {
        private Car car = new Car();

        public CarBuilder()
        {
            this.Reset();
        }

        public void ABS()
        {
            this.car.Add("ABS");
        }

        public void Car()
        {
            this.car.Add("Car");
        }

        public void CarDoors(int count)
        {
            this.car.Add("Car doors " + count.ToString());
        }

        public void MotionSensor()
        {
            this.car.Add("Sensors");
        }

        public void Radio()
        {
            this.car.Add("Radio");
        }

        public void Wheels()
        {
            this.car.Add("Wheels");
        }

        public void Reset()
        {
            this.car = new Car();
        }
        
    }

    public class Car
    {
        private List<string> listPartOfCar = new List<string>();

        public void Add(string part)
        {
            this.listPartOfCar.Add(part);
        }

        public void GetListOfParts()
        {
            StringBuilder listParts = new StringBuilder();

            foreach (string partsName in listPartOfCar)
            {
                listParts.AppendLine(partsName);
            }

            Console.WriteLine(listParts.ToString());
        }
    }

    public class Builder
    {
        public ICarBuilder _carBuilder { private get; set; }

        public void BasicCar()
        {
            this._carBuilder.Car();
            this._carBuilder.CarDoors(5);
            this._carBuilder.Wheels();
        }

        public void StandartCar()
        {
            this.BasicCar();
            this._carBuilder.Radio();
            this._carBuilder.ABS();
        }

        public void AdvancedCar()
        {
            this.BasicCar();
            this.StandartCar();
            this._carBuilder.MotionSensor();
        }

    }
}
